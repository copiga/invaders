#include <stdio.h>
#include <ncurses.h>
#include <stdbool.h>

#include <math.h>

#include "configs.h"
#include "structs.c"
#include "globals.c"

void gameloop(void);
void boundcheck(void);
void collisioncheck(void);
int findnextbullet(void);
void initplayerbullets(void);
void inertia(void);



int main(void)
{
  {
    initscr();/*initialise screen*/
    raw();/*do not require return to be pressed to enterkeys*/
    keypad(stdscr,TRUE);/*arrow keys are quite lovely are they not?*/
    noecho();/*do not tell user what they just entered*/
    getmaxyx(stdscr,maxy,maxx);
  }
  
  mvprintw((maxy/2),(maxx/2)-(49/2),"use the up or down keys to move and space to fire");/*the start formula are to fully center the text on screen*/
  refresh();
  getch();
  //                                                                                     nodelay();/*may be changed, this is to stop invaders from only moving every halfdelay()*/ NOT GOT ENOUGH PARAMS! FIX THIS
  halfdelay(1);/*temporary*/
  
  
  initplayerbullets();
  
  {
    y=maxy/2;
    x=maxx/8;
  }
  
  
  gameloop();


  endwin();
  return 0;
}


void gameloop(void)
{
  int input;
  int nextfireable=0;
  while((input=getch())!=27)/*27=='esc key'*/
    {
      {
	mvprintw(y,x," ");/*clear the old player "sprite"*/
	if(input==' ')
	  {
	    nextfireable=findnextbullet();
	    playerbullets[nextfireable].fired=true;
	    playerbullets[nextfireable].x=x+1;
	    playerbullets[nextfireable].y=y;
	    playerbullets[nextfireable].sprite=(rand()%2==0)?'~':'-';/*animation, may be removed...*/
	    printw("%c", playerbullets[nextfireable].sprite);
	  }
	else if(input==KEY_UP)
	  {
	    y-=1;/*0,0 is top left*/
	  }
	else if(input==KEY_DOWN)
	  {
	    y+=1;/*maxy,maxx==bottom right*/
	  }
	boundcheck();
	mvprintw(y,x,">");
      }
      inertia();
      collisioncheck();
      refresh();
    } 
}

void boundcheck(void)
{

  /*******************************
   *if x==maximum x then it is x-1*
   *if y==maximum y then it is y-1**
   **********************************/

  /********************
   *WORKING 01/01/2013*
   ********************/
  //getmaxyx(stdscr,y,x);
  
  if(maxy<y)
    {
      //  y=maxy-1;
      }
  if(0>y)
    {
      y=1;
    }
}

void collisioncheck(void)
{
  /*more will be added to this. honest...
  int c;
  for(c=0;c<=BULLETS_MAX;c++)
    {
      if(playerbullets[c].x<maxx)
	{
	  playerbullets[c].fired=false;
	  mvprintw(playerbullets[c].y,playerbullets[c].x,"%c",playerbullets[c].sprite);
	}
    }
  */
}

int findnextbullet(void)
{
      /*using a pretty terrible search algorithm but it might work*/
      int c;
      for(c=0;playerbullets[c].fired==true;c++);
      return c;
}

void initplayerbullets(void)
{
  int c;
  for(c=0;c<=BULLETS_MAX;c++)
    {
      playerbullets[c].fired=false;
      playerbullets[c].x=0;
	      playerbullets[c].y=0;
    }
}

void inertia(void)
{
  int c;
  for(c=0;c<=BULLETS_MAX;c++)
    {
      if(playerbullets[c].fired)
	{
	  playerbullets[c].oldy=playerbullets[c].y;
	  playerbullets[c].oldx=playerbullets[c].x;
	  playerbullets[c].x+=1;
	  mvprintw(playerbullets[c].oldy,playerbullets[c].oldx," ");
	  mvprintw(playerbullets[c].y,playerbullets[c].x,"%c",playerbullets[c].sprite);
	}
    }
  
}


/*tonight tonight im holding you tight, tomorrow you will be gone*//*FR-025*/
